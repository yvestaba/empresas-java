package br.com.ioasys.ioasysIMDb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.ioasys.ioasysIMDb.model.Perfil;
@Repository
public interface PerfilRepository extends JpaRepository<Perfil, Long>{

}
