package br.com.ioasys.ioasysIMDb.repository;


import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.ioasys.ioasysIMDb.model.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long>{
	
	public Page<Usuario> findAllByAtivoAndPerfilId(Boolean ativo, Long perfilId, Pageable pageable);
	public Optional<Usuario> findByNome(String nome);
	public List<Usuario> findAllByPerfilId(Long perfilId);

}
