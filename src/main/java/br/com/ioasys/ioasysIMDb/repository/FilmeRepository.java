package br.com.ioasys.ioasysIMDb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import br.com.ioasys.ioasysIMDb.model.Filme;

@Repository
public interface FilmeRepository extends JpaRepository<Filme, Long>, JpaSpecificationExecutor<Filme> {
	
	public List<Filme> findByNome(String nome);
	

}
