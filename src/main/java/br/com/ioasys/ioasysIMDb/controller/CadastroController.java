package br.com.ioasys.ioasysIMDb.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.ioasys.ioasysIMDb.dto.LoginForm;
import br.com.ioasys.ioasysIMDb.dto.Senha;
import br.com.ioasys.ioasysIMDb.dto.UsuarioDto;
import br.com.ioasys.ioasysIMDb.model.Usuario;
import br.com.ioasys.ioasysIMDb.repository.UsuarioRepository;
import br.com.ioasys.ioasysIMDb.service.CadastroService;

@RestController
@RequestMapping("cadastro")
public class CadastroController {



	@Autowired
	private UsuarioRepository ur;
	
	@Autowired
	private CadastroService cs;
	
	@Autowired
	private AuthenticationManager am;
	
	
	
	@GetMapping("{id}")
	public ResponseEntity<UsuarioDto> mostraUsuario (@PathVariable Long id) {
		Optional<Usuario> usuario = ur.findById(id);
		if(usuario.isPresent()) {
			return ResponseEntity.ok(new UsuarioDto(usuario.get()));
		}
		
		return ResponseEntity.badRequest().build();
	}
	
	
	
	@PostMapping("usuario")
	public ResponseEntity<UsuarioDto> novoUsuarioComum(@RequestBody @Valid LoginForm form, UriComponentsBuilder ucb) {
		Optional<Usuario> usuarioExistente = ur.findByNome(form.getNome());
		if (!usuarioExistente.isPresent()) {
			Usuario usuario = cs.cadastrar(2l, form);
			
			URI uri = ucb.path("/cadastro/{id}").buildAndExpand(usuario.getId()).toUri();
			return ResponseEntity.created(uri).body(new UsuarioDto(usuario));
		}

		return ResponseEntity.badRequest().build();
	}
	
	@PostMapping("primeiro_adm")
	public ResponseEntity<UsuarioDto> primeiroUsuarioAdm(@RequestBody @Valid LoginForm form, UriComponentsBuilder ucb){
		List<Usuario> usuarios = ur.findAllByPerfilId(1l);
		if(usuarios.size() > 0) {
			return ResponseEntity.badRequest().build();
		}
		Optional<Usuario> usuarioExistente = ur.findByNome(form.getNome());
		if (!usuarioExistente.isPresent()) {
			Usuario usuario = cs.cadastrar(1l, form);
			
			URI uri = ucb.path("/cadastro/{id}").buildAndExpand(usuario.getId()).toUri();
			return ResponseEntity.created(uri).body(new UsuarioDto(usuario));
		}

		return ResponseEntity.badRequest().build();
	}
	
	@PostMapping("adm")
	public ResponseEntity<UsuarioDto> novoUsuarioAdm(@RequestBody @Valid LoginForm form, UriComponentsBuilder ucb) {
		Optional<Usuario> usuarioExistente = ur.findByNome(form.getNome());
		if (!usuarioExistente.isPresent()) {
			Usuario usuario = cs.cadastrar(1l, form);
			
			URI uri = ucb.path("/cadastro/{id}").buildAndExpand(usuario.getId()).toUri();
			return ResponseEntity.created(uri).body(new UsuarioDto(usuario));
		}

		return ResponseEntity.badRequest().build();
	}
	
	@PatchMapping("desativar")
	public ResponseEntity<UsuarioDto> desativar(@RequestBody @Valid LoginForm form, Authentication aut) {
		UsernamePasswordAuthenticationToken dados = form.converter();
		
		try {
			am.authenticate(dados);
			Usuario usuario = ur.findById(((Usuario) aut.getPrincipal()).getId()).get();
			usuario.setAtivo(false);
			ur.save(usuario);
			
			return ResponseEntity.ok(new UsuarioDto(usuario));
			
		} catch (AuthenticationException e) {
			return ResponseEntity.badRequest().build();
		}
		
	}
	
	@PatchMapping("nova_senha")
	public ResponseEntity<?> novaSenha(@RequestBody @Valid Senha senha, Authentication aut){
		Long usuarioId = ((Usuario) aut.getPrincipal()).getId();
		Usuario usuario = ur.findById(usuarioId).get();
		usuario.setSenha(new BCryptPasswordEncoder().encode(senha.getSenha()));
		
		ur.save(usuario);
		
		return ResponseEntity.ok().build();
		
	}

}
