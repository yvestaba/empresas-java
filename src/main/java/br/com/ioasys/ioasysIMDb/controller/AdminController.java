package br.com.ioasys.ioasysIMDb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.ioasys.ioasysIMDb.dto.UsuarioDto;
import br.com.ioasys.ioasysIMDb.service.UsuarioService;

@RestController
@RequestMapping("/adm")
public class AdminController {

	
	@Autowired
	private UsuarioService service;
	
	@GetMapping("usuarios")
	public ResponseEntity<Page<UsuarioDto>> usuariosPorOrdemAlfabetica(@RequestParam(defaultValue = "0") int pagina, 
			@RequestParam(defaultValue = "4") int tamanho){	
		Page<UsuarioDto> retorno = service.listagemUsuarios(pagina, tamanho);
		return ResponseEntity.ok(retorno);
		
	}

}
