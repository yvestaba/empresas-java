package br.com.ioasys.ioasysIMDb.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.ioasys.ioasysIMDb.dto.FilmeDto;
import br.com.ioasys.ioasysIMDb.dto.FilmeForm;
import br.com.ioasys.ioasysIMDb.dto.Nota;
import br.com.ioasys.ioasysIMDb.model.Filme;
import br.com.ioasys.ioasysIMDb.model.Usuario;
import br.com.ioasys.ioasysIMDb.repository.FilmeRepository;
import br.com.ioasys.ioasysIMDb.repository.UsuarioRepository;
import br.com.ioasys.ioasysIMDb.service.FilmeService;
import br.com.ioasys.ioasysIMDb.specification.FilmeSpecification;

@RestController
@RequestMapping("filmes")
public class FilmeController {

	@Autowired
	private FilmeService service;

	@Autowired
	private FilmeRepository repository;
	
	@Autowired
	private UsuarioRepository ur;

	@GetMapping("{id}")
	public ResponseEntity<Filme> filmePorId(@PathVariable Long id) {
		Optional<Filme> filmeOptional = repository.findById(id);
		if (filmeOptional.isPresent()) {
			return ResponseEntity.ok(filmeOptional.get());
		}

		return ResponseEntity.badRequest().build();
	}

	@PutMapping("voto{id}")
	@Transactional
	public ResponseEntity<Filme> voto(@PathVariable Long id, @RequestBody Nota nota, UriComponentsBuilder ucb,
			Authentication aut) {
		Usuario usuario = (Usuario) aut.getPrincipal();
		Usuario transactional = ur.findById(usuario.getId()).get();
		
		Optional<Filme> filme = repository.findById(id);
		if(!filme.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		
		if(transactional.getFilmesVotados().stream()
				.anyMatch(f -> f.getId().equals(id))) {
			return ResponseEntity.badRequest().build();
		}
		
		service.votacao(filme.get(), nota.getNota(), transactional);
		

		return ResponseEntity.ok(filme.get());
	}

	@PostMapping("novo")
	public ResponseEntity<Filme> novo(@RequestBody @Valid FilmeDto novoFilme, UriComponentsBuilder ucb) {
		Filme filme = novoFilme.toFilme();
		repository.save(filme);

		URI uri = ucb.path("/filmes/{id}").buildAndExpand(filme.getId()).toUri();
		return ResponseEntity.created(uri).body(filme);

	}

	@PostMapping
	public ResponseEntity<Page<Filme>> filmesFiltrados(@RequestBody(required = false) FilmeForm dto) {
		
		Pageable paginacao = service.paginacaoPadrao(0, 20);
		
		if (dto != null) {
			Page<Filme> filmes = repository.findAll(Specification.where(FilmeSpecification.nome(dto.getNome()))
					.and(FilmeSpecification.diretor(dto.getDiretor())).and(FilmeSpecification.genero(dto.getGenero()))
					.and(FilmeSpecification.ator(dto.getAtor())), paginacao);

			return ResponseEntity.ok(filmes);
		}

		return ResponseEntity.badRequest().build();

	}
	
	@GetMapping
	public ResponseEntity<Page<Filme>> todosOsFilmes(@RequestParam(defaultValue = "0") int pagina,
			@RequestParam(defaultValue = "20") int tamanho){
		
		
		Pageable paginacao = service.paginacaoPadrao(pagina, tamanho);
		
		return ResponseEntity.ok(repository.findAll(paginacao));
		
	}

}
