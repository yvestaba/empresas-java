package br.com.ioasys.ioasysIMDb.service;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ioasys.ioasysIMDb.dto.LoginForm;
import br.com.ioasys.ioasysIMDb.model.Perfil;
import br.com.ioasys.ioasysIMDb.model.Usuario;
import br.com.ioasys.ioasysIMDb.repository.PerfilRepository;
import br.com.ioasys.ioasysIMDb.repository.UsuarioRepository;

@Service
public class CadastroService {
	
	@Autowired
	private PerfilRepository pr;
	
	@Autowired
	private UsuarioRepository ur;

	public Usuario cadastrar(Long idAuthority,  LoginForm form) {
		List<Perfil> autoridades = new ArrayList<Perfil>();
		autoridades.add(pr.findById(idAuthority).get());
		
		Usuario novoUsuario = form.converter(autoridades);
		ur.save(novoUsuario);
		return novoUsuario;
	}

}
