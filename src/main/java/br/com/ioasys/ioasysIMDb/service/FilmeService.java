package br.com.ioasys.ioasysIMDb.service;


import javax.transaction.Transactional;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import br.com.ioasys.ioasysIMDb.model.Filme;
import br.com.ioasys.ioasysIMDb.model.Usuario;
import br.com.ioasys.ioasysIMDb.repository.FilmeRepository;
import br.com.ioasys.ioasysIMDb.repository.UsuarioRepository;

@Service
public class FilmeService {

	@Autowired
	private FilmeRepository repository;

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	
	@Transactional
	public Filme votacao(Filme filme, @Min(0) @Max(4) Integer nota, Usuario usuario) {

		filme.votaNota(nota);
		
		usuario.getFilmesVotados().add(filme);
		
		repository.save(filme);
		usuarioRepository.save(usuario);
		
		
		return filme;

	}


	public Pageable paginacaoPadrao(int pagina, int tamanho) {
		Sort sort = Sort.by(Direction.DESC, "totalVotos").and(Sort.by(Direction.ASC, "nome"));
		Pageable paginacao = PageRequest.of(pagina, tamanho, sort);
		return paginacao;
	}

}
