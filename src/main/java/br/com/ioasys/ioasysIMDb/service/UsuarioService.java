package br.com.ioasys.ioasysIMDb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import br.com.ioasys.ioasysIMDb.dto.UsuarioDto;
import br.com.ioasys.ioasysIMDb.model.Usuario;
import br.com.ioasys.ioasysIMDb.repository.UsuarioRepository;

@Service
public class UsuarioService {
	
	@Autowired
	private UsuarioRepository repository;

	public Page<UsuarioDto> listagemUsuarios(int pagina, int tamanho) {
		Pageable paginacao = PageRequest.of(pagina, tamanho, Direction.ASC, "nome");

		Page<Usuario> usuarios = repository.findAllByAtivoAndPerfilId(true, 2l, paginacao);

		
		return UsuarioDto.converter(usuarios);
	}

}
