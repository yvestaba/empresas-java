package br.com.ioasys.ioasysIMDb.specification;

import org.springframework.data.jpa.domain.Specification;

import br.com.ioasys.ioasysIMDb.model.Filme;
import br.com.ioasys.ioasysIMDb.model.Genero;

public class FilmeSpecification {

	public static Specification<Filme> nome(String nome) {
		return (root, criteriaQuery, criteriaBuilder) -> {
			if (nome == null) {
				return criteriaBuilder.conjunction();
			}
			return criteriaBuilder.like(root.get("nome"), "%" + nome + "%");
		};
	}

	public static Specification<Filme> diretor(String diretor) {
		return (root, criteriaQuery, criteriaBuilder) -> {
			if (diretor == null) {
				return criteriaBuilder.conjunction();
			}

			return criteriaBuilder.like(root.get("diretor"), "%" + diretor + "%");
		};
	}

	public static Specification<Filme> genero(Genero genero) {
		return (root, criteriaQuery, criteriaBuilder) -> {
			if (genero == null) {
				return criteriaBuilder.conjunction();
			}
			return criteriaBuilder.equal(root.get("genero"), genero);
		};
	}

	public static Specification<Filme> ator(String ator) {
		return (root, criteriaQuery, criteriaBuilder) -> {
			if (ator == null) {
				return criteriaBuilder.conjunction();
			}
			return criteriaBuilder.like(root.join("atores"), "%" + ator + "%");
		};
	}

}
