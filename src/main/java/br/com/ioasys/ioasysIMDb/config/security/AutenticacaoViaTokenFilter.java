package br.com.ioasys.ioasysIMDb.config.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import br.com.ioasys.ioasysIMDb.model.Usuario;
import br.com.ioasys.ioasysIMDb.repository.UsuarioRepository;

public class AutenticacaoViaTokenFilter extends OncePerRequestFilter {
	
	private TokenService ts;
	private UsuarioRepository ur;
	
	public AutenticacaoViaTokenFilter(TokenService ts, UsuarioRepository ur) {
		this.ts = ts;
		this.ur = ur;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		String token = this.recuperarToken(request);
		boolean valido = ts.ehUmTokenValido(token);
		if(valido) {
			this.autenticarUsuario(token);
		}
		
		filterChain.doFilter(request, response);
	}

	private void autenticarUsuario(String token) {
		Long idUsuario = ts.getIdUsuario(token);
		Usuario usuario = ur.findById(idUsuario).get();
		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(usuario, null, usuario.getAuthorities());
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
	}

	private String recuperarToken(HttpServletRequest request) {
		String cabecalho = request.getHeader("Authorization");
		if(cabecalho == null || cabecalho.isEmpty() || !cabecalho.startsWith("Bearer ")) {
			return null;
		}
		return cabecalho.substring(7, cabecalho.length());
	}

}
