package br.com.ioasys.ioasysIMDb.config.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import br.com.ioasys.ioasysIMDb.repository.UsuarioRepository;
import br.com.ioasys.ioasysIMDb.service.AutenticacaoService;

@EnableWebSecurity
@Configuration
@Profile(value =  {"prod", "dev"})
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private AutenticacaoService as;
	
	@Autowired
	private TokenService ts;
	
	@Autowired
	private UsuarioRepository ur;
	
	@Override
	@Bean
	protected AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager();
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(as).passwordEncoder(new BCryptPasswordEncoder());
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/adm/*").hasAuthority("admin")
		.antMatchers(HttpMethod.POST, "/filmes/novo").hasAuthority("admin")
		.antMatchers(HttpMethod.PUT, "/filmes/*").hasAuthority("usuario")
		.antMatchers(HttpMethod.GET, "/filmes").permitAll()
		.antMatchers("/filmes/*").authenticated()
		.antMatchers(HttpMethod.POST, "/auth").permitAll()
		.antMatchers("/cadastro/adm").hasAuthority("admin")
		.antMatchers("/cadastro/*").permitAll()
		.and().csrf().disable()
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		.and().addFilterBefore(new AutenticacaoViaTokenFilter(this.ts, this.ur), UsernamePasswordAuthenticationFilter.class)
		;
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring()
        .antMatchers("/**.html", "/v2/api-docs", "/webjars/**", "/configuration/**", "/swagger-resources/**");
	}

}
