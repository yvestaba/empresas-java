package br.com.ioasys.ioasysIMDb.config.security;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import br.com.ioasys.ioasysIMDb.model.Usuario;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenService {
	
	@Value("${imdb.jwt.secret}")
	private String senha;

	public String gerarToken(Authentication authentication) {
		Usuario logado = (Usuario) authentication.getPrincipal();
		
		Date hoje = new Date();
		Date expiracao = new Date(hoje.getTime() + 86400000l);
		
		return Jwts.builder()
				.setIssuer("imdb (Yves Taba)")
				.setSubject(logado.getId().toString())
				.setIssuedAt(hoje)
				.setExpiration(expiracao)
				.signWith(SignatureAlgorithm.HS256, senha)
				.compact()
				;
	}

	public boolean ehUmTokenValido(String token) {
		try {
			Jwts.parser().setSigningKey(this.senha).parseClaimsJws(token);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public Long getIdUsuario(String token) {
		Claims claims = Jwts.parser().setSigningKey(this.senha).parseClaimsJws(token).getBody();
		return Long.parseLong(claims.getSubject());
	}

}
