package br.com.ioasys.ioasysIMDb.dto;

import java.util.List;

import javax.validation.constraints.NotBlank;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.ioasys.ioasysIMDb.model.Perfil;
import br.com.ioasys.ioasysIMDb.model.Usuario;

public class LoginForm {
	@NotBlank
	private String nome;
	@NotBlank
	private String senha;
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public String getNome() {
		return nome;
	}
	public UsernamePasswordAuthenticationToken converter() {		
		return new UsernamePasswordAuthenticationToken(nome, senha);
	}
	
	public Usuario converter(List<Perfil> autoridades) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		Usuario usuario = new Usuario();
		usuario.setNome(nome);
		String senhaBCrypt = encoder.encode(senha);
		usuario.setSenha(senhaBCrypt);
		usuario.setPerfil(autoridades);
		usuario.setAtivo(true);
		return usuario;
	}
	
	
}
