package br.com.ioasys.ioasysIMDb.dto;

import org.springframework.data.domain.Page;

import br.com.ioasys.ioasysIMDb.model.Usuario;

public class UsuarioDto {
	
	private String nome;
	
	public UsuarioDto(Usuario usuario) {
		this.nome = usuario.getUsername();
	}
	
	public static Page<UsuarioDto> converter(Page<Usuario> usuarios){
		return usuarios.map(UsuarioDto::new);
	}
	
	public String getNome() {
		return nome;
	}
}
