package br.com.ioasys.ioasysIMDb.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.com.ioasys.ioasysIMDb.model.Filme;
import br.com.ioasys.ioasysIMDb.model.Genero;

public class FilmeDto {
	
	@NotBlank
	private String nome;
	@NotBlank
	private String diretor;
	@NotNull
	private Genero genero;
	@NotEmpty
	private List<String> atores = new ArrayList<String>();
	
	public Filme toFilme() {
		Filme filme = new Filme();
		filme.setNome(nome);
		filme.setDiretor(diretor);
		filme.setGenero(genero);
		filme.setAtores(atores);
		
		return filme;
	}
	
	
	public String getNome() {
		return nome;
	}
	public String getDiretor() {
		return diretor;
	}
	public Genero getGenero() {
		return genero;
	}
	public List<String> getAtores() {
		return atores;
	}
	
	

}
