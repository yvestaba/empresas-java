package br.com.ioasys.ioasysIMDb.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class Nota {
	
	@Min(value = 0l) @Max(value = 4l)
	private Integer nota;
	
	public Integer getNota() {
		return nota;
	}

}
