package br.com.ioasys.ioasysIMDb.dto;

import javax.validation.constraints.NotBlank;

public class Senha {
	
	@NotBlank
	private String senha;
	
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public String getSenha() {
		return senha;
	}

}
