package br.com.ioasys.ioasysIMDb.dto;

import br.com.ioasys.ioasysIMDb.model.Genero;

public class FilmeForm {
	
	
	private String nome;	
	private String diretor;	
	private Genero genero;	
	private String ator;

	public String getNome() {
		return nome;
	}

	public String getDiretor() {
		return diretor;
	}

	public Genero getGenero() {
		return genero;
	}

	public String getAtor() {
		return ator;
	}
	
	

}
