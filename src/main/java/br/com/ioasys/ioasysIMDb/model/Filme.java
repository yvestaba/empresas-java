package br.com.ioasys.ioasysIMDb.model;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;

@Entity
public class Filme {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String nome;
	
	private Long notas0 = 0l;
	private Long notas1 = 0l;
	private Long notas2 = 0l;
	private Long notas3 = 0l;
	private Long notas4 = 0l;
	private Long totalVotos = 0l;
	private Double mediaVotos = 0.0;
	private String diretor;
	@Enumerated(EnumType.STRING)
	private Genero genero;
	@ElementCollection
	@JoinTable(name= "filme_atores", joinColumns = @JoinColumn(name="id_filme"))
	@Column(name="ator")
	private List<String> atores;

	public void votaNota(int nota) {
		if(nota == 0) {
			notas0++;
		} else if(nota == 1) {
			notas1++;
		} else if(nota == 2) {
			notas2++;
		} else if(nota == 3) {
			notas3++;
		} else if(nota == 4) {
			notas4++;
		} else {
			throw new IllegalStateException("Somente numeros entre 0 e 4 sao permitidos para a votacao");
		} 
		totalVotos++;
		
		this.calcularMedia();
	}
	
	private void calcularMedia() {
		Long somaDasNotas = notas1 + (notas2 * 2) + (notas3 * 3) + (notas4 * 4);
		DecimalFormat formato = new DecimalFormat("#.#", new DecimalFormatSymbols(Locale.US));
		
		Double valorSemConversao = (Double.valueOf(somaDasNotas)) / totalVotos;
		
		mediaVotos = Double.valueOf(formato.format(valorSemConversao));
	}
	
	public Long getTotalVotos() {
		return totalVotos;
	}
	
	public Double getMediaVotos() {
		return mediaVotos;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
	
	public Long getId() {
		return id;
	}

	public Long getNotas0() {
		return notas0;
	}

	public Long getNotas1() {
		return notas1;
	}

	public Long getNotas2() {
		return notas2;
	}

	public Long getNotas3() {
		return notas3;
	}

	public Long getNotas4() {
		return notas4;
	}

	public String getDiretor() {
		return diretor;
	}

	public Genero getGenero() {
		return genero;
	}

	public List<String> getAtores() {
		return atores;
	}

	public void setDiretor(String diretor) {
		this.diretor = diretor;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public void setAtores(List<String> atores) {
		this.atores = atores;
	}
	
	
	
	
	
	

}
