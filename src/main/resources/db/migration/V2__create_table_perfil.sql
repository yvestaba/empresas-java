CREATE TABLE PERFIL (
    ID BIGINT AUTO_INCREMENT NOT NULL,
    NOME VARCHAR(20) NOT NULL,
    CONSTRAINT PK_PERFIL PRIMARY KEY (ID),
    CONSTRAINT U_PERFIL_NOME UNIQUE (NOME)
);