CREATE TABLE USUARIO_VOTO (
	ID BIGINT AUTO_INCREMENT NOT NULL,
    ID_USUARIO BIGINT NOT NULL,
    ID_FILME BIGINT NOT NULL,
    CONSTRAINT PK_USUARIO_VOTO PRIMARY KEY (ID),
    CONSTRAINT FK_USUARIO_VOTO_USUARIO FOREIGN KEY (ID_USUARIO) REFERENCES USUARIO (ID),
	CONSTRAINT FK_USUARIO_VOTO_FILME FOREIGN KEY (ID_FILME) REFERENCES FILME (ID)
);