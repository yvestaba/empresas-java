package br.com.ioasys.ioasysIMDb.controller;


import java.net.URI;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("dev")
public class AutenticacaoControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void criarUsuario() throws Exception {
		URI uri = new URI("/cadastro/primeiro_adm");
		String json = "{\r\n"
				+ "    \"nome\": \"yves\",\r\n"
				+ "    \"senha\": \"ioasys\"\r\n"
				+ "}";
		mockMvc.perform(MockMvcRequestBuilders.post(uri)
				.content(json)
				.contentType(MediaType.APPLICATION_JSON));
	}

	@Test
	public void loginDoUsuarioQueFoiCriadoComMockMvc() throws Exception {
		URI uri = new URI("/auth");
		String json = "{\r\n"
				+ "    \"nome\": \"yves\",\r\n"
				+ "    \"senha\": \"ioasys\"\r\n"
				+ "}";
		
		mockMvc.perform(MockMvcRequestBuilders.post(uri)
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().is(200));
	}
	
	@Test
	public void loginDoUsuarioQueFoiCriadoComDadosErrados() throws Exception {
		URI uri = new URI("/auth");
		String json = "{\r\n"
				+ "    \"nome\": \"yvess\",\r\n"
				+ "    \"senha\": \"ioassssys\"\r\n"
				+ "}";
		
		mockMvc.perform(MockMvcRequestBuilders.post(uri)
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().is(400));
	}

}
