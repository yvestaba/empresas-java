package br.com.ioasys.ioasysIMDb.controller;

import static org.junit.jupiter.api.Assertions.*;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class AdminControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void contextLoads() {
	}

	@Test
	public void retornaUsuarioCriado() throws Exception {
		URI uri = new URI("/cadastro/usuario");
		String json = "{\r\n" + "    \"nome\": \"um.usuario\",\r\n" + "    \"senha\": \"ioasys\"\r\n" + "}";
		mockMvc.perform(MockMvcRequestBuilders.post(uri).content(json)
				.contentType(MediaType.APPLICATION_JSON));

		uri = new URI("/adm/usuarios");
		mockMvc.perform(MockMvcRequestBuilders.get(uri))
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
				.andDo(MockMvcResultHandlers.print());

	}

}
