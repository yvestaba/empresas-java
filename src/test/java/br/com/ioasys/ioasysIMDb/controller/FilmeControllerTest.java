package br.com.ioasys.ioasysIMDb.controller;

import static org.junit.jupiter.api.Assertions.*;

import java.net.URI;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import br.com.ioasys.ioasysIMDb.repository.FilmeRepository;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class FilmeControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private FilmeRepository repository;

	@Test
	public void contextLoads() {
	}

	@Test
	public void cadastraNovoFilme() throws Exception {
		URI uri = new URI("/filmes/novo");
		String json = "{\r\n"
				+ "	\"nome\": \"jogos de damas\",\r\n"
				+ "	\"diretor\": \"joaquim\",\r\n"
				+ "	\"genero\": \"SUSPENSE\",\r\n"
				+ "	\"atores\": [\r\n"
				+ "		\"miguel\",\r\n"
				+ "		\"sabrina\",\r\n"
				+ "		\"fortunato\",\r\n"
				+ "		\"paula\",\r\n"
				+ "		\"israel\"\r\n"
				+ "	]\r\n"
				+ "}";
		try {
		mockMvc.perform(MockMvcRequestBuilders.post(uri).content(json)
				.contentType(MediaType.APPLICATION_JSON));
		} catch(Exception e) {
			
		}
		
		assertNotNull(repository.findByNome("jogo de damas"));
				
	}
	
	@Test
	public void todosOsFilmes() throws Exception {
		URI uri = new URI("/filmes");
		mockMvc.perform(MockMvcRequestBuilders.get(uri))
		.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));
	}
	
	@Test
	public void filtrarFilme() throws Exception {
		URI uri = new URI("/filmes");
		String json = "{\r\n"
				+ "	\"nome\": null,\r\n"
				+ "	\"diretor\": \"bruno\",\r\n"
				+ "	\"genero\": null,\r\n"
				+ "	\"ator\": null\r\n"
				+ "}";
		mockMvc.perform(MockMvcRequestBuilders.post(uri).content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().is(200));
	}

}
