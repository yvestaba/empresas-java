package br.com.ioasys.ioasysIMDb.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class FilmeTest {
	
	Filme filme = new Filme();

	@Test
	void quantidadeDeVotacoesEMedia() {
		filme.votaNota(0);
		filme.votaNota(1);
		filme.votaNota(2);
		filme.votaNota(3);
		filme.votaNota(4);
		
		assertEquals(5l, filme.getTotalVotos());
		assertEquals(2.0, filme.getMediaVotos());
	}
	
	@Test
	void quantidadeDeVotacoesEMediaComVotosDiferentes() {
		filme.votaNota(0);
		filme.votaNota(1);
		filme.votaNota(2);
		filme.votaNota(3);
		filme.votaNota(3);
		filme.votaNota(4);
		filme.votaNota(4);
		filme.votaNota(4);
		filme.votaNota(4);
		filme.votaNota(4);
		
		assertEquals(10l, filme.getTotalVotos());
		assertEquals(2.9, filme.getMediaVotos());
	}
	
	@Test
	void quantidadeDeVotacoesEMediaComNumeroQuebrado() {
		filme.votaNota(0);
		filme.votaNota(1);
		filme.votaNota(2);
		filme.votaNota(3);
		filme.votaNota(3);
		filme.votaNota(4);
		filme.votaNota(4);
		filme.votaNota(4);
		filme.votaNota(4);
		
		assertEquals(9l, filme.getTotalVotos());
		assertEquals(2.8, filme.getMediaVotos());
	}

}
